import Vue from 'vue';
import master from './components/layouts/master.vue';
import router from './router';
import store from './store';
import BootstrapVue from 'bootstrap-vue';

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue);

import axios from 'axios';
import VueAxios from 'vue-axios';

Vue.use(VueAxios, axios);

//new sidebar
import 'vue-sidebar-menu/dist/vue-sidebar-menu.css'
import VueSidebarMenu from 'vue-sidebar-menu'

Vue.use(VueSidebarMenu)

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { library } from "@fortawesome/fontawesome-svg-core";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";
Vue.component('font-awesome-icon', FontAwesomeIcon)
library.add(faSpinner);

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(master)
}).$mount('#app')
