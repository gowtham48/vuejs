import Vue from 'vue'
import VueRouter from 'vue-router'
import list from '../components/views/list.vue';
import form from '../components/views/form.vue';
import home from '../components/views/home.vue';

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: home
  },
  {
    path: '/list',
    name: 'list',
    component: list
  },
  {
    path: '/form',
    name: 'form',
    component: form
  }
 
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
